//
//  ViewController.swift
//  PitchHero-Test
//
//  Created by Russell Warwick on 11/6/18.
//  Copyright © 2018 Russell Warwick. All rights reserved.
//

import UIKit
import FacebookLogin
import RealmSwift
import Realm

class LoginVC: UIViewController {

    //MARK: - View Life Cycle
    
    override func viewDidLoad() {
        let loginButton = LoginButton(readPermissions: [.pagesManageCta ])
        loginButton.center = view.center
        loginButton.delegate = self
        view.addSubview(loginButton)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkAccessToken()
    }
    
    private func checkAccessToken(){
        if let token = FBSDKAccessToken.current() {
            let userPagesVC = UserPagesVC(nibName: UserPagesVC.identifier, bundle: nil)
            self.navigationController?.show(userPagesVC, sender: self)
        }
    }
}

extension LoginVC : LoginButtonDelegate {
    
    //MARK: - LoginButtonDelegate
    
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
        print("logged in")
    }
    
    func loginButtonDidLogOut(_ loginButton: LoginButton) {
        print("logged out")
    }

}
