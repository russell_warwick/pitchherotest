//
//  UIView.swift
//  PitchHero-Test
//
//  Created by Russell Warwick on 11/6/18.
//  Copyright © 2018 Russell Warwick. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    class func nib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

extension UITableViewCell {
    static var nib : UINib { return UINib(nibName: identifier, bundle: nil) }
    static var identifier: String { return String(describing: self) }
}

extension UIViewController {
    static var nib : UINib { return UINib(nibName: identifier, bundle: nil) }
    static var identifier: String { return String(describing: self) }
}
