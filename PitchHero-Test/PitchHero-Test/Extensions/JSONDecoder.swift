//
//  JSONDecoder.swift
//  PitchHero-Test
//
//  Created by Russell Warwick on 11/6/18.
//  Copyright © 2018 Russell Warwick. All rights reserved.
//

import Foundation

typealias JSON = [String : Any]

extension JSONDecoder {
    func decode<T: Decodable>(_ type: T.Type, from object: JSON) throws -> T {
        let data = try JSONSerialization.data(withJSONObject: object, options:[])
        return try decode(T.self, from: data)
    }
}
