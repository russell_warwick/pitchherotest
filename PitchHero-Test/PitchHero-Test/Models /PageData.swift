//
//  PageData.swift
//  PitchHero-Test
//
//  Created by Russell Warwick on 11/6/18.
//  Copyright © 2018 Russell Warwick. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class PageData : Object {
    
    @objc dynamic var name : String!
    @objc dynamic var id : String!
    @objc dynamic var userID : String!

    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(name: String, id: String, userID : String) {
        self.init()
        self.id = id
        self.name = name
        self.userID = userID
    }
    
}
