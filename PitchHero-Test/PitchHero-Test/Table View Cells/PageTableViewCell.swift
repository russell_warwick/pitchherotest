//
//  PageTableViewCell.swift
//  PitchHero-Test
//
//  Created by Russell Warwick on 11/6/18.
//  Copyright © 2018 Russell Warwick. All rights reserved.
//

import UIKit

class PageTableViewCell: UITableViewCell {

    //MARK: - Properties
    
    @IBOutlet var nameLabel : UILabel!
    @IBOutlet var idLabel : UILabel!
    
    //MARK: - Configure
    
    func configure(withVM vm : PageVM){
        nameLabel.text = vm.name
        idLabel.text = vm.id
    }
    
}
