//
//  AlertView.swift
//  MyToolKit
//
//  Created by Russell Warwick on 12/01/2018.
//  Copyright © 2018 Russell Warwick All rights reserved.
//

import Foundation
import UIKit


func showAlertView(_ viewController: UIViewController, message: String, type: AlertBarType = .success) {
    let options = AlertBar.Options(shouldConsiderSafeArea: true, isStretchable: false)
    AlertBar.show(type: type, message:message, duration: 2.5, options: options, completion: {})
}

