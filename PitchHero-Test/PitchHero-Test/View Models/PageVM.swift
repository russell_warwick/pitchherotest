//
//  PageVM.swift
//  PitchHero-Test
//
//  Created by Russell Warwick on 11/6/18.
//  Copyright © 2018 Russell Warwick. All rights reserved.
//

import Foundation

struct PageVM {
    
    let pageData : PageData!
    
    var name : String {
        return pageData.name.uppercased()
    }
    
    var id : String {
        return "ID: \(pageData.id!)"
    }
    
}
