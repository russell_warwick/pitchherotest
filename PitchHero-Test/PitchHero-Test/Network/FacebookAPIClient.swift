//
//  NetworkManager.swift
//  PitchHero-Test
//
//  Created by Russell Warwick on 11/6/18.
//  Copyright © 2018 Russell Warwick. All rights reserved.
//

import Foundation
import FacebookLogin
import RealmSwift

enum ApplicationError: Error {
    case unknown
    case failedRequest
    case noUserID
}

protocol FacebookAPIClientDelegate : class {
    func pagesFromLocal(pages : [PageData])
    func pagesFromNetwork(pages : [PageData])
    func errorDidOccur(error : ApplicationError)
}

class FacebookAPIClient {
    
    //MARK: -
    
    public var delegate : FacebookAPIClientDelegate?
    private var realm = try! Realm()
    
    //MARK: -
    
    func retrieveUsersGroups(){
        
        guard let userID = FBSDKAccessToken.current().userID else { delegate?.errorDidOccur(error: .noUserID) ; return }
        
        let pages = realm.objects(PageData.self).filter("userID = '\(userID)'")
        delegate?.pagesFromLocal(pages: Array(pages))

        FBSDKGraphRequest(graphPath: "/me/accounts", parameters: ["fields" : "name, id"]).start { (connection, results, error) in
            if let error = error {
                self.delegate?.errorDidOccur(error: .failedRequest)
            }
            if let results = results as? JSON, let pagesData = results["data"] as? [JSON] {
                
                let pages = pagesData.compactMap({ (json) -> PageData? in
                    if let name = json["name"] as? String, let id = json["id"] as? String {
                        let page = PageData(name: name, id: id,userID : userID)
                        self.cache(page: page)
                        return page
                    }
                    else{ fatalError("Could not decode PageData") }
                })
                self.delegate?.pagesFromNetwork(pages : pages)
            }
        }
    }

    private func cache(page : PageData){
        if realm.object(ofType: PageData.self, forPrimaryKey: page.id) == nil {
            try! realm.write {
                realm.add(page)
            }
        }
    }
}

